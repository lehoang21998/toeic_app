package com.application.dmt.toeicguru.utils;


import androidx.fragment.app.Fragment;

import org.greenrobot.eventbus.EventBus;

public class BaseFragment extends Fragment {
    @Override
    public void onResume() {
        super.onResume();
        String className = getClass().getSimpleName();
        EventBus.getDefault().post(new GetIndexEvenBusMenu(className));
    }

}
