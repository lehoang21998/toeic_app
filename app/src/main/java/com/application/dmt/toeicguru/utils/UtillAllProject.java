package com.application.dmt.toeicguru.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class UtillAllProject {
    public static boolean checkConnection(Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connMgr != null) {
            NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();

            if (activeNetworkInfo != null) { // connected to the internet
                // connected to the mobile provider's data plan
                if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                    // connected to wifi
                    return true;
                } else return activeNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE;
            }
        }
        return false;
    }
    public static String AdddecripstionLesson(Integer resultTamThoi,Integer scoreMax,String datetime){
        JSONObject decription=new JSONObject();
        try {
            decription.put("scoreMax",scoreMax);
            decription.put("resultTamThoi",resultTamThoi);
            decription.put("datetime",datetime);
            Log.d("Hasshcode",String.valueOf(decription));
            return String.valueOf(decription);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null ;
    }
}
