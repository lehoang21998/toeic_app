package com.application.dmt.toeicguru.ui.real_tests;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.application.dmt.toeicguru.utils.BaseFragment;
import com.application.dmt.toeicguru.R;

public class frag_Real_Test extends BaseFragment {
    View view;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_real_test, container, false);

        return view;
    }


}