package com.application.dmt.toeicguru.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "Question",
        foreignKeys = {@ForeignKey(entity = Category.class, parentColumns = "id", childColumns = "categoryId"),
                         @ForeignKey(entity = Question.class,parentColumns = "id",childColumns = "parentId"),
                         @ForeignKey(entity = Lesson.class,parentColumns = "id",childColumns = "lessonId")})
public class Question {
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    private Integer id;
    @NonNull
    @ColumnInfo(name = "lessonId")
    private Integer lessonId;
    @ColumnInfo(name = "type")
    private String type;
    @ColumnInfo(name = "description")
    private String description;
    @ColumnInfo(name = "content")
    private String content;
    @ColumnInfo(name = "mark")
    private Float mark;
    @ColumnInfo(name = "answer")
    private String answer;
    @ColumnInfo(name = "parentId")
    private Integer parentId;
    @ColumnInfo(name = "categoryId")
    private Integer categoryId;
    @ColumnInfo(name = "orderInLesson")
    private Integer orderInLesson;

    public Question(Integer lessonId, String type, String description, String content, Float mark, String answer, Integer parentId, Integer categoryId, Integer orderInLesson) {
        this.lessonId = lessonId;
        this.type = type;
        this.description = description;
        this.content = content;
        this.mark = mark;
        this.answer = answer;
        this.parentId = parentId;
        this.categoryId = categoryId;
        this.orderInLesson = orderInLesson;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLessonId() {
        return lessonId;
    }

    public void setLessonId(Integer lessonId) {
        this.lessonId = lessonId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Float getMark() {
        return mark;
    }

    public void setMark(Float mark) {
        this.mark = mark;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getOrderInLesson() {
        return orderInLesson;
    }

    public void setOrderInLesson(Integer orderInLesson) {
        this.orderInLesson = orderInLesson;
    }
}
