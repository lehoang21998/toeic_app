package com.application.dmt.toeicguru.ui.rate;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.application.dmt.toeicguru.R;
import com.application.dmt.toeicguru.utils.BaseFragment;

public class frag_Rate extends BaseFragment {
    View view;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_rate, container, false);

        return view;
    }

}
