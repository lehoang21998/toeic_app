package com.application.dmt.toeicguru.ui.frag_premium;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.application.dmt.toeicguru.utils.BaseFragment;
import com.application.dmt.toeicguru.R;

public class frag_Premium extends BaseFragment {
    View view;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_premium, container, false);

        return view;
    }

}
