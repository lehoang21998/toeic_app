package com.application.dmt.toeicguru.ui.practices.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.application.dmt.toeicguru.R;
import com.application.dmt.toeicguru.model.ChildCategory;
import com.application.dmt.toeicguru.ui.practices.EventBus.CategoryEventBus;


import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class AdapterCategory extends RecyclerView.Adapter<AdapterCategory.ViewHodel> {
    private Context context;

    public AdapterCategory(Context context) {
        this.context = context;
    }

    private List<ChildCategory> listPart;
    private static final int TYPE_READING=5;
    private static final int TYPE_LISTENING=0;
    private static final int TYPE_LIST=1;
    public void setData(List<ChildCategory> listPart){
        this.listPart=listPart;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public ViewHodel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType==TYPE_LISTENING){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lession_toeic,parent,false);
            return new ViewHodel(view,viewType);
        }
        else if(viewType==TYPE_READING){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lession_toeic,parent,false);
            return new ViewHodel(view,viewType);
        }
        else if(viewType==TYPE_LIST){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_partquestion_layout,parent,false);
            return new ViewHodel(view,viewType);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHodel holder, int position) {
        if(holder.view_type==TYPE_LISTENING){
            holder.txtPartLession.setText("LISTENING");
        }
        else if(holder.view_type==TYPE_READING){
            holder.txtPartLession.setText("READING");
        }
        else if(holder.view_type==TYPE_LIST){
            ChildCategory childCategory;
            if(position>5){
                childCategory =listPart.get(position-2);
            }else childCategory =listPart.get(position-1);
            holder.setItem(childCategory);



            //ADD DATA CATEGORY CHỖ NÀY
            holder.imageView.setImageResource(childCategory.getIdImageView());
            holder.txtPart.setText(childCategory.getPartTittle());
            holder.txtNamePart.setText(childCategory.getNameTittle());
            holder.progressBar.setProgress(100);
            //...
        }
    }

    @Override
    public int getItemCount() {
        return listPart.size()+2;
    }

    public class ViewHodel extends RecyclerView.ViewHolder {
        int view_type;
        private ImageView imageView;
        private ProgressBar progressBar;
        private TextView txtPart,txtNamePart,txtPhan,txtphanTram;
        private TextView txtPartLession;
        private ChildCategory childCategory;

        public ViewHodel(@NonNull View itemView,int viewType) {
            super(itemView);
            if(viewType==TYPE_LISTENING){
                txtPartLession=itemView.findViewById(R.id.txtPartLession);
                view_type=0;
            }
            else if(viewType==TYPE_READING){
                txtPartLession=itemView.findViewById(R.id.txtPartLession);
                view_type=5;
            }
            else if(viewType==TYPE_LIST){
                imageView=itemView.findViewById(R.id.imageView);
                progressBar=itemView.findViewById(R.id.progressBarphantramhoc);
                txtPart=itemView.findViewById(R.id.txtpart);
                txtNamePart=itemView.findViewById(R.id.txtnamepart);
                txtPhan=itemView.findViewById(R.id.txtphanhoc);
                txtphanTram=itemView.findViewById(R.id.txtphantramhoc);
                view_type=1;
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        EventBus.getDefault().post(new CategoryEventBus(childCategory));
                    }
                });
            }
        }

        public void setItem(ChildCategory childCategory) {
            this.childCategory=childCategory;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(position==0) return TYPE_LISTENING;
        else if(position==5) return TYPE_READING;
        else return TYPE_LIST;
    }
}
