package com.application.dmt.toeicguru.ui.practices.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.application.dmt.toeicguru.ui.practices.EventBus.LessonDtaEventBus;
import com.application.dmt.toeicguru.R;
import com.application.dmt.toeicguru.model.Lesson;
import com.skydoves.progressview.ProgressView;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class AdapterLesson extends RecyclerView.Adapter<AdapterLesson.LessonViewHolder>{
    private List<Lesson> lessonList;
    private Context mContext;


    public AdapterLesson(Context mContext ) {
        this.mContext = mContext;
    }

    public void setDataLesson(List<Lesson> lessonList){
        this.lessonList=lessonList;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public LessonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_item_lesson_in_category, parent, false);
        return new LessonViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull LessonViewHolder holder, int position) {
        Lesson lesson = lessonList.get(position);
        //        public Lesson( @NonNull Integer categoryId, String name, String description
//                , Integer questionCount, Integer resultTamThoi, Integer scoreMax, String datetime) {
        String description=lesson.getDescription();
        Integer countquestion=lesson.getQuestionCount();



        //BA TRẠNG THÁI CỦA LESSON
        if(description==null){
            holder.textViewName.setText(lesson.getName());
            holder.imageView.setImageResource(R.drawable.lesson_icon_default);
            holder.textViewNumberQuestion.setText(countquestion + " QUESTION");
        }else{
            try {
                JSONObject jsonRoot = new JSONObject(description);
                Integer Scoremax=jsonRoot.getInt("scoreMax");
                Integer scroretamthoi=jsonRoot.getInt("resultTamThoi");
                String datetimefinish=jsonRoot.getString("datetime");
                if(datetimefinish==null){
                    holder.textViewName.setText(lesson.getName());
                    holder.imageView.setImageResource(R.drawable.lesson_icon_doing);
                    holder.progressBar.setVisibility(View.VISIBLE);
                    holder.progressBar.setProgress(scroretamthoi);
                    holder.textViewNumberQuestion.setVisibility(View.INVISIBLE);
                }
                else{
                    holder.relativeLayout.setVisibility(View.INVISIBLE);
                    holder.relativeLayoutEnd.setVisibility(View.VISIBLE);
                    holder.textViewName.setText(lesson.getName());
                    holder.txtScoreMax.setText(Scoremax+"/"+countquestion);
                    holder.txtDatefinish.setText(datetimefinish);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        holder.setItem(lesson);
    }

    @Override
    public int getItemCount() {
        return lessonList.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class LessonViewHolder extends RecyclerView.ViewHolder{
        TextView textViewName;
        TextView textViewNumberQuestion,txtScoreMax,txtDatefinish;
        ImageView imageView;
        ProgressView progressBar;
        RelativeLayout relativeLayoutEnd,relativeLayout;
        private Lesson lesson;

        public LessonViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName            =itemView.findViewById(R.id.nameLesson);
            textViewNumberQuestion  =itemView.findViewById(R.id.textViewNumberQuestion);
            imageView               =itemView.findViewById(R.id.image_showStatusLesson);
            progressBar             =itemView.findViewById(R.id.progressViewLesson);
            relativeLayoutEnd       =itemView.findViewById(R.id.layout_done_lesson);
            relativeLayout          =itemView.findViewById(R.id.layoutTW);
            txtScoreMax             =itemView.findViewById(R.id.textViewNumberDone);
            txtDatefinish           =itemView.findViewById(R.id.textViewGetDateDone);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post(new LessonDtaEventBus(lesson));
                }
            });
        }

        public void setItem(Lesson lesson) {
            this.lesson=lesson;
        }
    }
}
