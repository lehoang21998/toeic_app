package com.application.dmt.toeicguru.ui.practices.fragment;

import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.application.dmt.toeicguru.database.RoomDatabaseStore;
import com.application.dmt.toeicguru.model.ChildCategory;
import com.application.dmt.toeicguru.model.Lesson;
import com.application.dmt.toeicguru.ui.practices.adapter.AdapterLesson;
import com.application.dmt.toeicguru.utils.BaseFragment;
import com.application.dmt.toeicguru.R;

import java.util.ArrayList;
import java.util.List;

public class frag_Lesson_In_Category_all extends BaseFragment {
    View view;
    RecyclerView recyclerViewLesson;
    AdapterLesson adapterLesson;
    List <Lesson> lessonList;
    private ChildCategory childCategory;

    public frag_Lesson_In_Category_all(ChildCategory childCategory) {
        this.childCategory=childCategory;
    }



    @Override
    public void onStart() {
        super.onStart();
        Log.d("AAAAA",childCategory.getNameTittle());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_lesson_in_category_all, container, false);
        //Code here
        initUI();


        return view;
    }

    private void initUI() {
        recyclerViewLesson  = view.findViewById(R.id.recyclerViewLessonAll);
        adapterLesson = new AdapterLesson(getContext());
        adapterLesson.setDataLesson(getDataLesson());
        recyclerViewLesson.setLayoutManager(new GridLayoutManager(view.getContext(),2));
        recyclerViewLesson.setHasFixedSize(true);
        recyclerViewLesson.setAdapter(adapterLesson);
    }

    private List<Lesson> getDataLesson() {
//        public Lesson( @NonNull Integer categoryId, String name, String description
//                , Integer questionCount, Integer resultTamThoi, Integer scoreMax, String datetime) {
        lessonList  = new ArrayList<>();
        lessonList= RoomDatabaseStore.getInstance(getContext()).lessonDAO().getListLesson(childCategory.getId());
//        lessonList.add(new Lesson(5,"#1",null,20,5,null,null));
//        lessonList.add(new Lesson(5,"#2",null,20,null,2,"NOV 6"));
//        lessonList.add(new Lesson(5,"#3",null,10,null,null,null));
//        lessonList.add(new Lesson(5,"#1",null,20,5,null,null));
//        lessonList.add(new Lesson(5,"#2",null,20,null,10,"NOV 6"));
//        lessonList.add(new Lesson(5,"#3",null,10,null,null,null));
//        lessonList.add(new Lesson(5,"#1",null,20,5,null,null));
//        lessonList.add(new Lesson(5,"#2",null,20,null,19,"NOV 6"));
//        lessonList.add(new Lesson(5,"#3",null,10,null,null,null));
//        lessonList.add(new Lesson(5,"#1",null,20,5,null,null));
//        lessonList.add(new Lesson(5,"#2",null,20,null,17,"NOV 6"));
//        lessonList.add(new Lesson(5,"#3",null,10,null,null,null));
//        lessonList.add(new Lesson(5,"#1",null,20,5,null,null));
//        lessonList.add(new Lesson(5,"#2",null,20,null,17,"NOV 6"));
//        lessonList.add(new Lesson(5,"#3",null,10,null,null,null));

        return lessonList;
    }

}