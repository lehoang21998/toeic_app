package com.application.dmt.toeicguru.ui.practices.EventBus;

import com.application.dmt.toeicguru.model.ChildCategory;

public class CategoryEventBus {
    private ChildCategory childCategory;
    public CategoryEventBus(ChildCategory childCategory) {
        this.childCategory=childCategory;
    }

    public ChildCategory getChildCategory() {
        return childCategory;
    }
}
