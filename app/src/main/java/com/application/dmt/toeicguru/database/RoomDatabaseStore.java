package com.application.dmt.toeicguru.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.application.dmt.toeicguru.model.Category;
import com.application.dmt.toeicguru.model.Lesson;
import com.application.dmt.toeicguru.model.Question;
import com.application.dmt.toeicguru.model.Result;
import com.application.dmt.toeicguru.model.User;
import com.application.dmt.toeicguru.model.UserAnswer;

@Database(entities = {Category.class, Lesson.class,Question.class,Result.class,User.class,UserAnswer.class}
                    ,version = 2)
public abstract class RoomDatabaseStore extends RoomDatabase {
    private static final String DATABASE_NAME= "toeicguru";
    private static RoomDatabaseStore instance;
    public static synchronized RoomDatabaseStore getInstance(Context context){
        if(instance == null)
        {
            instance = Room.databaseBuilder(context.getApplicationContext(), RoomDatabaseStore.class,DATABASE_NAME)
                    .allowMainThreadQueries()
                    .createFromAsset("toeicdatabase.db")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }


    public abstract categoryDAO categoryDAO();
    public abstract lessonDAO lessonDAO();
    public abstract questionDAO questionDAO();
    public abstract resultDAO resultDAO();
    public abstract userDAO userDAO();
    public abstract useranswerDAO useranswerDAO();
}
