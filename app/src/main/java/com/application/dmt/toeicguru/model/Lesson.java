package com.application.dmt.toeicguru.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

@Entity(tableName ="Lesson",
        foreignKeys = @ForeignKey(entity = Category.class,
        parentColumns = "id",
        childColumns = "categoryId"))
public class Lesson {
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    private Integer id;
    @NonNull
    @ColumnInfo(name = "categoryId")
    private Integer categoryId;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "description")
    private String description;
    @ColumnInfo(name = "questionCount")
    private Integer questionCount;
    @Ignore
    private Integer resultTamThoi;
    @Ignore
    private Integer scoreMax;// số câu đạt được trong 1 lesson
    @Ignore
    private String datetime;
    public Lesson(@NotNull Integer categoryId, String name, String description, Integer questionCount) {
        this.categoryId = categoryId;
        this.name = name;
        this.description = description;
        this.questionCount = questionCount;
    }

    public Lesson( @NonNull Integer categoryId, String name, String description
            , Integer questionCount, Integer resultTamThoi, Integer scoreMax, String datetime) {

        this.categoryId = categoryId;
        this.name = name;
        this.description = description;
        this.questionCount = questionCount;
        this.resultTamThoi = resultTamThoi;
        this.scoreMax = scoreMax;
        this.datetime = datetime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getQuestionCount() {
        return questionCount;
    }

    public void setQuestionCount(Integer questionCount) {
        this.questionCount = questionCount;
    }

    public Integer getResultTamThoi() {
        return resultTamThoi;
    }

    public void setResultTamThoi(Integer resultTamThoi) {
        this.resultTamThoi = resultTamThoi;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public Integer getScoreMax() {
        return scoreMax;
    }

    public void setScoreMax(Integer scoreMax) {
        this.scoreMax = scoreMax;
    }
}
