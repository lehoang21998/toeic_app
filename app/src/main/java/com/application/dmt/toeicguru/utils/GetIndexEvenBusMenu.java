package com.application.dmt.toeicguru.utils;

public class GetIndexEvenBusMenu {
    private  String className;

    public GetIndexEvenBusMenu(String className) {
        this.className=className;
    }

    public String getClassName() {
        return className;
    }
}
