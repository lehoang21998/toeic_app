package com.application.dmt.toeicguru.ui.practices.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.application.dmt.toeicguru.R;
import com.application.dmt.toeicguru.model.ChildCategory;
import com.application.dmt.toeicguru.ui.practices.adapter.AdapterCategory;
import com.application.dmt.toeicguru.utils.BaseFragment;

import java.util.ArrayList;
import java.util.List;

public class frag_Practice extends BaseFragment {
    private View view;
    private RecyclerView recyclerViewUI;
    private AdapterCategory adapterCategory;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_practices, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        recyclerViewUI = view.findViewById(R.id.recyclerview);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerViewUI.setLayoutManager(linearLayoutManager);
        recyclerViewUI.setHasFixedSize(true);
        adapterCategory = new AdapterCategory(getContext());
        adapterCategory.setData(getDataChildCategory());

        recyclerViewUI.setAdapter(adapterCategory);
    }



    private List<ChildCategory> getDataChildCategory() {
        List<ChildCategory> childCategoryList = new ArrayList<>();
        childCategoryList.add(new ChildCategory(5,R.drawable.icon_category_photo,"Part 1","Photographs",12,50));
        childCategoryList.add(new ChildCategory(6,R.drawable.icon_category_question_response,"Part 2","Question-Response",null,50));
        childCategoryList.add(new ChildCategory(7,R.drawable.icon_category_short_conversations,"Part 3","Short Conversations",null,50));
        childCategoryList.add(new ChildCategory(8,R.drawable.icon_category_talks,"Part 4","Talks",50,50));
        childCategoryList.add(new ChildCategory(9,R.drawable.icon_category_incompleted_sentences,"Part 5","Incomplete Sentences",null,50));
        childCategoryList.add(new ChildCategory(10,R.drawable.icon_category_text_completion,"Part 6","Text Completion",50,50));
        childCategoryList.add(new ChildCategory(11,R.drawable.icon_category_reading_comprehension,"Part 7","Reading Comprehension",50,50));
        return childCategoryList;
    }




}