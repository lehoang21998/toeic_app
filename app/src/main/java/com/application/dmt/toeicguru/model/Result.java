package com.application.dmt.toeicguru.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName ="Result",
        foreignKeys = {@ForeignKey(entity = Lesson.class, parentColumns = "id", childColumns = "lessonId"),
                @ForeignKey(entity = User.class,parentColumns = "id",childColumns = "userId")})
//FOREIGN KEY("lessonId") REFERENCES "Lesson"("id"),
//	FOREIGN KEY("userId") REFERENCES "User"("id"),
public class Result {
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    private Integer id;
    @NonNull
    @ColumnInfo(name = "userId")
    private Integer userId;
    @NonNull
    @ColumnInfo(name = "lessonId")
    private Integer lessonId;
    @ColumnInfo(name = "duration")
    private Integer duration;
    @ColumnInfo(name = "progress")
    private Integer progress;
    @ColumnInfo(name = "resultScore")
    private String resultScore;
    @ColumnInfo(name = "latestQuestionId")
    private Integer latestQuestionId;
    @ColumnInfo(name = "lastTimeVisited")
    private Integer lastTimeVisited;

    public Result(Integer userId, Integer lessonId, Integer duration, Integer progress, String resultScore, Integer latestQuestionId, Integer lastTimeVisited) {
        this.userId = userId;
        this.lessonId = lessonId;
        this.duration = duration;
        this.progress = progress;
        this.resultScore = resultScore;
        this.latestQuestionId = latestQuestionId;
        this.lastTimeVisited = lastTimeVisited;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getLessonId() {
        return lessonId;
    }

    public void setLessonId(Integer lessonId) {
        this.lessonId = lessonId;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getProgress() {
        return progress;
    }

    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    public String getResultScore() {
        return resultScore;
    }

    public void setResultScore(String resultScore) {
        this.resultScore = resultScore;
    }

    public Integer getLatestQuestionId() {
        return latestQuestionId;
    }

    public void setLatestQuestionId(Integer latestQuestionId) {
        this.latestQuestionId = latestQuestionId;
    }

    public Integer getLastTimeVisited() {
        return lastTimeVisited;
    }

    public void setLastTimeVisited(Integer lastTimeVisited) {
        this.lastTimeVisited = lastTimeVisited;
    }
}
