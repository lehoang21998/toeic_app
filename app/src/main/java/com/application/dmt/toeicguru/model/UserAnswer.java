package com.application.dmt.toeicguru.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "UserAnswer",
        foreignKeys = {@ForeignKey(entity = Result.class, parentColumns = "id", childColumns = "resultId"),
                @ForeignKey(entity = Question.class,parentColumns = "id",childColumns = "questionId")})
public class UserAnswer {
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    private Integer id;
    @NonNull
    @ColumnInfo(name = "resultId")
    private Integer resultId;
    @NonNull
    @ColumnInfo(name = "questionId")
    private Integer questionId;
    @ColumnInfo(name = "duration")
    private Integer duration;
    @ColumnInfo(name = "answer")
    private String answer;
    @ColumnInfo(name = "mark")
    private Float mark;

    public UserAnswer(Integer resultId, Integer questionId, Integer duration, String answer, Float mark) {
        this.resultId = resultId;
        this.questionId = questionId;
        this.duration = duration;
        this.answer = answer;
        this.mark = mark;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getResultId() {
        return resultId;
    }

    public void setResultId(Integer resultId) {
        this.resultId = resultId;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Float getMark() {
        return mark;
    }

    public void setMark(Float mark) {
        this.mark = mark;
    }
}
