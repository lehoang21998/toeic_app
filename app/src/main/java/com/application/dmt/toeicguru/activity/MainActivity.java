package com.application.dmt.toeicguru.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;


import com.application.dmt.toeicguru.ui.practices.EventBus.CategoryEventBus;
import com.application.dmt.toeicguru.ui.practices.EventBus.LessonDtaEventBus;
import com.application.dmt.toeicguru.ui.practices.fragment.frag_Practice;
import com.application.dmt.toeicguru.ui.rate.frag_Rate;
import com.application.dmt.toeicguru.ui.settings.frag_Setting;
import com.application.dmt.toeicguru.ui.share.frag_Share;
import com.application.dmt.toeicguru.utils.GetIndexEvenBusMenu;
import com.google.android.material.navigation.NavigationView;
import com.application.dmt.toeicguru.R;
import com.application.dmt.toeicguru.ui.frag_premium.frag_Premium;
import com.application.dmt.toeicguru.ui.online_store.frag_Online_Store;
import com.application.dmt.toeicguru.ui.practices.fragment.frag_Lesson_In_Category_all;
import com.application.dmt.toeicguru.ui.practices.fragment.frag_Travel_Category;
import com.application.dmt.toeicguru.ui.real_tests.frag_Real_Test;
import com.application.dmt.toeicguru.ui.statistics.frag_Statistics;
import com.application.dmt.toeicguru.ui.word_book.frag_Word_Book;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;
    public static NavigationView navigationView;
    private FrameLayout frameLayout;
    public static HashMap<String, Integer> fragmentPositions = new HashMap<String, Integer>();
    static {
        fragmentPositions.put("frag_Practice", 0);
        fragmentPositions.put("frag_Lesson_In_Category_all", 0);
        fragmentPositions.put("frag_Travel_Category", 0);


        fragmentPositions.put("frag_Real_Test", 1);
        fragmentPositions.put("frag_Word_Book", 2);
        fragmentPositions.put("frag_Statistics", 3);
        fragmentPositions.put("frag_Online_Store", 4);
        fragmentPositions.put("frag_Setting", 5);
        fragmentPositions.put("frag_Share", 6);
        fragmentPositions.put("frag_Rate", 7);
        fragmentPositions.put("frag_Premium", 8);

    }

    private String classname;
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void SetCheckedNavigationDrawer(GetIndexEvenBusMenu event){
        classname=event.getClassName();
        navigationView.getMenu().getItem(fragmentPositions.get(classname)).setChecked(true);
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getDatafromAdapterCategory(CategoryEventBus event){
        frag_Lesson_In_Category_all fragLessonInCategoryAll =new frag_Lesson_In_Category_all(event.getChildCategory());
                        FragmentTransaction transaction=
                                                         getSupportFragmentManager()
                                                        .beginTransaction()
                                                        .setCustomAnimations(R.anim.anim_left_to_rignt,R.anim.anim_exit_left_to_right);
                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        transaction.replace(R.id.nav_host_fragment, fragLessonInCategoryAll).addToBackStack(null);
                        transaction.commit();
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getDataLessonToTravel(LessonDtaEventBus event){
        frag_Travel_Category frag_travel_category =new frag_Travel_Category(event.getLesson());
        FragmentTransaction transaction=
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.anim_left_to_rignt,R.anim.anim_exit_left_to_right);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.nav_host_fragment, frag_travel_category).addToBackStack(null);
        transaction.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        frameLayout=findViewById(R.id.nav_host_fragment);


        navigationView.setNavigationItemSelectedListener(MainActivity.this);
        ActionBarDrawerToggle toggle=new ActionBarDrawerToggle(this,drawer,toolbar,R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        if (savedInstanceState==null){
            inTenFragment(new frag_Practice());
            navigationView.setCheckedItem(R.id.nav_practices);
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_practices:
                inTenFragment(new frag_Practice());
                break;
            case R.id.nav_real_tests:
                inTenFragment(new frag_Real_Test());
                break;
            case R.id.nav_word_book:
                inTenFragment(new frag_Word_Book());
                break;
            case R.id.nav_statistics:
                inTenFragment(new frag_Statistics());
                break;
            case R.id.nav_onl_store:
                inTenFragment(new frag_Online_Store());
                break;
            case R.id.nav_setting:
                inTenFragment(new frag_Setting());
                break;
            case R.id.nav_share:
                inTenFragment(new frag_Share());
                break;
            case R.id.nav_rate:
                inTenFragment(new frag_Rate());
                break;
            case R.id.nav_premium:
                inTenFragment(new frag_Premium());
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void inTenFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.anim_left_to_rignt, R.anim.anim_exit_left_to_right)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.nav_host_fragment,fragment)
                .addToBackStack(null)
                .commit();
    }

}