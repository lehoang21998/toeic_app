package com.application.dmt.toeicguru.model;

public class ChildCategory {
    private int id;
    private int idImageView;
    private String partTittle;
    private String nameTittle;
    private Integer accumulation;
    private Integer maxlessSon;

    public ChildCategory(int id, int idImageView, String partTittle, String nameTittle, Integer accumulation, Integer maxlessSon) {
        this.id = id;
        this.idImageView = idImageView;
        this.partTittle = partTittle;
        this.nameTittle = nameTittle;
        this.accumulation = accumulation;
        this.maxlessSon = maxlessSon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdImageView() {
        return idImageView;
    }

    public void setIdImageView(int idImageView) {
        this.idImageView = idImageView;
    }

    public String getPartTittle() {
        return partTittle;
    }

    public void setPartTittle(String partTittle) {
        this.partTittle = partTittle;
    }

    public String getNameTittle() {
        return nameTittle;
    }

    public void setNameTittle(String nameTittle) {
        this.nameTittle = nameTittle;
    }

    public Integer getAccumulation() {
        return accumulation;
    }

    public void setAccumulation(Integer accumulation) {
        this.accumulation = accumulation;
    }

    public Integer getMaxlessSon() {
        return maxlessSon;
    }

    public void setMaxlessSon(Integer maxlessSon) {
        this.maxlessSon = maxlessSon;
    }
}
