package com.application.dmt.toeicguru.ui.settings;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.DialogTitle;

import com.application.dmt.toeicguru.ui.practices.fragment.frag_Practice;
import com.application.dmt.toeicguru.R;
import com.application.dmt.toeicguru.utils.BaseFragment;
import com.application.dmt.toeicguru.utils.GetIndexEvenBusMenu;

import org.greenrobot.eventbus.EventBus;

public class frag_Setting extends BaseFragment {
    View view;
    LinearLayout linear_Translate, linear_Target, linear_Audio, linear_Word_Access, linear_Update;
    TextView textView_Language_DictionNary, textView_Word_Access, textView_Time_Update;
    CheckBox checkBoxTranslate, checkBoxAudio;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings, container, false);
        initUI();
        onClickLinear();

        return view;
    }

    private void initUI() {
        linear_Translate    = view.findViewById(R.id.linear_layout_tap_translate);
        linear_Target       = view.findViewById(R.id.linear_layout_target_language);
        linear_Audio        = view.findViewById(R.id.linear_layout_audio);
        linear_Word_Access  = view.findViewById(R.id.linear_layout_word_access_mode);
        linear_Update       = view.findViewById(R.id.linear_layout_update_interval);

        textView_Language_DictionNary   = view.findViewById(R.id.textView_language_dictionary);
        textView_Word_Access            = view.findViewById(R.id.textView_Word_Access);
        textView_Time_Update            = view.findViewById(R.id.textView_Time_Update);

        checkBoxTranslate   = view.findViewById(R.id.checkbox_tap_translate);
        checkBoxAudio       = view.findViewById(R.id.checkbox_tap_audio);
    }

    //Event onclick item linear layout on fragment settings
    private void onClickLinear(){
        linear_Translate.setOnClickListener(v ->
                checkBoxTranslate.setChecked(!checkBoxTranslate.isChecked()));
        linear_Audio.setOnClickListener(v ->
                checkBoxAudio.setChecked(!checkBoxAudio.isChecked()));

        //onclick linear layout Target language for Dictionary
        linear_Target.setOnClickListener(v -> showLanguage());

        //onclick linear layout word access mode
        linear_Word_Access.setOnClickListener(v -> showWordAccess());

        //onclick linear layout update interval
        linear_Update.setOnClickListener(v -> showTimeUpdate());

    }

    private void showTimeUpdate() {
        AlertDialog.Builder alert = new AlertDialog.Builder(view.getContext());
        alert.create();
        alert.setTitle(R.string.word_access_mode);
        String []listItem = new String[]{"15 minutes","30 minutes","1 hour","2 hour","3 hours",
                "5 hours","8 hours","13 hours","21 hours","1 day"};
        alert.setSingleChoiceItems(listItem,1,(dialog, which) -> {
            textView_Time_Update.setText(listItem[which]);
            dialog.dismiss();
        });
        alert.setNegativeButton("Cancel",null);
        alert.show();
    }

    private void showWordAccess() {
        AlertDialog.Builder alert = new AlertDialog.Builder(view.getContext());
        alert.create();
        alert.setTitle(R.string.word_access_mode);
        String []listItem = new String[]{"Sequence","Random"};
        alert.setSingleChoiceItems(listItem,0,(dialog, which) -> {
            textView_Word_Access.setText(listItem[which]);
            dialog.dismiss();
        });
        alert.setNegativeButton("Cancel",null);
        alert.show();
    }

    @SuppressLint("SetTextI18n")
    private void showLanguage() {
        AlertDialog.Builder alert = new AlertDialog.Builder(view.getContext());
        alert.create();
        String []listItem = new String[]{"Afrikaans","Albanian","Arabic","Azerbaijani","Basque",
                "Bengali","Belarusian","Catalan","Chinese Simplified","Chinese Traditional",
                "Croatian","Czech","Danish","Dutch","English","Esperanto","Estonian",
                "Filipino","Finnish","French","Galician","Georgian","Greek","Gujarati",
                "Hindi","Hungarian","Icelandic","Indonesian","Irish","Italian","Japanese","Kannada",
                "Korea","Latin","Latvian","Malay","Thai","Vietnamese"};
        alert.setSingleChoiceItems(listItem, 14, (dialog, which) -> {
            textView_Language_DictionNary.setText(listItem[which]);
            dialog.dismiss();
        });
        @SuppressLint("RestrictedApi")
        DialogTitle dialogTitle = new DialogTitle(view.getContext());
        dialogTitle.setGravity(Gravity.CENTER);
        dialogTitle.setTextColor(Color.BLUE);
        dialogTitle.setTextSize(24);
        dialogTitle.setPadding(0,20,0,20);
        dialogTitle.setText("Select Target Language");
        alert.setCustomTitle(dialogTitle);


        alert.setNegativeButton("Cancel", null);
        alert.show();
    }
    @Override
    public void onResume() {
        super.onResume();
        String className = frag_Practice.class.getSimpleName();
        EventBus.getDefault().post(new GetIndexEvenBusMenu(className));
    }
}
