package com.application.dmt.toeicguru.database;

import androidx.room.Dao;
import androidx.room.Query;

import com.application.dmt.toeicguru.model.Category;

import java.util.List;

@Dao
public interface categoryDAO {
    @Query("SELECT * FROM Category ")
    List<Category> getListCategory();
}
