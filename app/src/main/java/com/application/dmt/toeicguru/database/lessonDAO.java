package com.application.dmt.toeicguru.database;

import androidx.room.Dao;
import androidx.room.Query;

import com.application.dmt.toeicguru.model.Lesson;

import java.util.List;

@Dao
public interface lessonDAO {
    @Query("SELECT * FROM Lesson WHERE categoryId =  :idCategory")
    List<Lesson> getListLesson(Integer idCategory);
}
