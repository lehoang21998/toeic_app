package com.application.dmt.toeicguru.ui.practices.fragment;

import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.application.dmt.toeicguru.model.Lesson;
import com.application.dmt.toeicguru.utils.BaseFragment;
import com.application.dmt.toeicguru.R;


public class frag_Travel_Category extends BaseFragment {


    private Lesson lesson;

    public frag_Travel_Category() {
        // Required empty public constructor
    }

    public frag_Travel_Category(Lesson lesson) {
        this.lesson=lesson;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("AAAAA",lesson.getName());
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_frag__travel__category, container, false);
    }

}