package com.application.dmt.toeicguru.ui.practices.EventBus;

import com.application.dmt.toeicguru.model.Lesson;

public class LessonDtaEventBus {
    private Lesson lesson;

    public LessonDtaEventBus(Lesson lesson) {
        this.lesson = lesson;
    }

    public Lesson getLesson() {
        return lesson;
    }
}
