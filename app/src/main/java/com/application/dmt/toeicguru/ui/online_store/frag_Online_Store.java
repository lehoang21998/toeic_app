package com.application.dmt.toeicguru.ui.online_store;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.application.dmt.toeicguru.utils.BaseFragment;
import com.application.dmt.toeicguru.R;

public class frag_Online_Store extends BaseFragment {
    View view;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_onl_store, container, false);

        return view;
    }

}
